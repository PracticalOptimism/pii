# 🥧 Pii - Personal Internet Infrastructure


Things to do:

- [] Need to talk about creating super users (also known as servers) using glitch for connecting peers together using webrtc, websocket or http
- [] Need to talk about a websocket server for each peer? (using glitch)
- [] Need to talk about database providers like firebase mongodb and providing credentials to these service providers to store data


Work in Progress (Planning related):
- [] Need to write a plan for how to develop pii
  - [x] already have a gitlab repository and npm package released to test public access for other people
  - [x] already have this current readme.md document for people to see the latest planning and whatever is being done for this project
  - [x] inspired by the data synchronization library Braid protocol by toomim (https://stateb.us/, https://github.com/braid-work/toomim-braidjs)
  - [x] inspired by the data storage and file storage library js-ipfs (https://github.com/ipfs/js-ipfs)
  - [x] inspired by the multiple provider resource allocators by piusnyakoojo (https://gitlab.com/enoplay-org/com-enoplay-www/-/tree/master/src%2Fservice%2Fprovider)
  - [x] inspired by peer-to-peer file streaming protocol webtorrent by feross (https://github.com/webtorrent/webtorrent)
  - [x] inspired by world computer idea by Vitalik Buterin to have decentralized computing
  - [] thinking about one import for pii and it should synchronize data across automatically networked peers as well as the components for any individual peer to read and write data from the pii library
  - [] thinking about having the library work both as a server data syncronization library as well as a browser client data synchronization library


Work in Progress (Architecture related):
- [] Using new architecture to achieve pii
  - [x] already using provider, consumer, usecase architecture
  - [] still determining how to use multiple credentials routing
  - [] still determining how to generate api for javascript, http and websocket clients

Work in Progress (Scheduler related):
- [] Need to talk about multiple frequency schedulers
  - [x] already thought to make functions that periodically perform computer operations at a given frequency (for example once ever 1 hour or once every 2 minutes or once every 10 seconds.. etc..)
  - [x] already thought about the benefits of schedulers as used in game networking operations like scheduling networking between clients and servers but it's already useful to improve performance in other operations as well .. essentially.. all other computer operations as i could imagine.. have a scheduler for everything?!
  - [x] already thought about having multiple schedulers that handle different time scales of operations like once every 2 weeks could be a useful scheduler to have if you need notifications for doctor's appointments. once every 2 seconds is useful for maybe a game like chess with a friend online. once every 2 milliseconds is useful for realtime 3D world multiplayer boss battles with zegodhand from Berserk.. if that MMO existed
  - [] write function to creating schedulers set at a given frequency to apply operations from a queue
  - [] write function to delete schedulers
  - [] thinking do you need to update a scheduler?.. maybe update it's frequency?.. 
  - [] thinking about what the minimum and maximum frequency cycles should be (100 milliseconds minimum? 10 years maximum? haha)
- [] Need to talk about queueing operations for schedulers
  - [] something about computation budgets needs to be addressed
    - [] thinking about using a operation queue to track the operations a user performs for later processing
    - [] thinking not sure.. something about queues and tables of operations associated with those queues.. the queue needs to be repeatedly generated based on a table of operations since those operations can be updated to occur at later dates at any time.. so the solution seems to be have a table of operations and have a scheduler look at that table to re-order a list (or queue) of operations.. the queue is sorted by the date the operations are supposed to be applied.. and if this is across multiple computers.. that can be a funny operation to make since you have to look at a global table .. or in other worlds a distributed hash table .. or maybe not a distributed hash table.. but certainly a hash table across multiple peers that need to communicate about who is responsible for completing the operation.. for example if it's an operation to upload a picture to the internet.. you can queue that operation or place it in a list and it doesn't necessarily need to be performed by the computer you're using.. but can be performed by your neighbors computer that runs the code to upload the image.. but you don't necessarily want to upload the image multiple times from several different computers.. they need to coordinate if the operation has been performed or not and that's where the hashgraph comes in.. so it seems like we can probably track .. using the hashgraph.. who has what operations.. and who has what responsibilities for applying those operations.. so as part of the global state object of the hashgraph.. we keep track of who has the operations to do XYZ.. or perhaps.. right.. maybe i should say something about why we're needing more than computer to have the operation since that's kind of confusing to me as well.. hmm.. i think it has something to do with redundancy.. we want to have the operations be completeled with high probability.. so in case any one computer shuts down from the network.. we have more computers that have the operation and can know if the operation has been completed by its peers or not.. if it hasn't then that computer communicates with its peer on who's responsibility it is to pick up the mantle in case the original computer who had that responsibility goes offline.. 
  - [] place operation computations in a queue before processing by a scheduler


Work in Progress (Hashgraph related):
- [x] already building hashgraph library (based on ts-hashgraph) using a new software architecture for speed of development
- [x] already thought about the usefulness of credentials as a way of toggling between multiple providers in the new software architecture
- [] still thinking about using hashgraph as the default networking library for the pii library to find peers and synchronize application state as well as sending and receiving operational transforms.. or operations for people to compute at scheduled times..
  - [] the scheduler is a big deal it seems, do we have a standard format for what the data structure looks like for a scheduler? scheduledDateToProcess? dateProcessed? howManyTimesToProcess? operationID? creatorDID (distributed identifier for the creator of this operation)? cancelOperation? processorID? dateCancelled? can there be multiple processors?.. well if we have the hashgraph maybe that'a avoidable?.. it might not be avoidable to the nanosecond.. if for example the operation is supposed to happen very soon across multiple computers that know about the operation.. well.. if we have to visit this idea again.. we'll do so.. 
  - [] Does the hashgraph need a scheduler?.. or only the pii library?>.. well certainly the hashgraph shard for any peer running the algorithm has at least one scheduler running at let's say.. a frequency of once every second.. so it's good enough to play chess.. or even a game like .. hmm.. something that's not as good as world of warcraft.. but a lot better to go faster than chess games with slow players.. so maybe playing chess with Magnus Carlsen.. 1 second moves.. :O 
- [] Need to talk about automatically trying to re-connect to peers when connection is lost
- [] Need to talk about using the hashgraph to synchronize operations between peers
- [] Need to talk about using the hashgraph to synchronize the state of the application on a single client browser
- [] Need to talk about using the hashgraph to syncrhonize the global state of the world across many shards made of individual client browsers and super peers


Work in progress (Cryptography related):
- [x] already thought about using public key cryptography to secure sending messages to one another as peers across the internet
- [x] already thought about using the distributed identifiers (did) standard to associate the the algorithm responsible for processing the key like 'ed25519:absdfuwoeirwoejrwoerworosfdsdf' something like that.. 
- [] Need to talk about cryptography such as elliptic npm library to create unique identities for each user using public-key cryptography
- [] 